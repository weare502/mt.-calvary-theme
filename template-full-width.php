<?php
/**
 * Template Name: Full Width - No Sidebar
 *
 * @package Mt. Calvary Lutheran Church
 */

get_header(); ?>

<?php 
	$hero = get_field('page_photo');
	if ( !empty($hero) ) :
		$hero = get_field('page_photo')['sizes']['page-photo']; 
	endif;
	if ( empty($hero) ){
		$hero = get_field('page_photo_fallback', 'options')['sizes']['page-photo'];
	}
?>
	<img src="<?php echo $hero; ?>" class="hero">

	<div id="primary" class="content-area full-width">

		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						//comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
