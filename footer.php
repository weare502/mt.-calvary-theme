<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Mt. Calvary Lutheran Church
 */
?>

	</div><!-- #content -->

	<!-- <div class="social">
		<p class="facebook"><a href="https://wwcw.facebook.com/MtCalvaryLutheranChurch" class="fa fa-facebook-square">Stay connected with <br/><span>Mt. Calvary Lutheran Church</span></a></p>
		<p class="email">Feel free to contact us with your questions. <br> <br> <a href="<?php echo get_permalink(123); ?>" class="button">E-Mail Us</a></p>
	</div> -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<!-- <nav class="footer-nav">
			<?php wp_nav_menu('footer'); ?>
		</nav> -->

		<div class="container footer-info">
			<div class="footer-contact">
				<p>
				<strong>Mt. Calvary Lutheran Church</strong><br>
				17535 Say Rd <br>
				Wamego, KS 66547<br>
				<a href="tel:(785) 456-2444">(785) 456-2444</a><br>
				<a href="mailto:mclc.wamego@gmail.com">mclc.wamego@gmail.com</a>
				</p>
			</div>
			<iframe class="footer-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3091.0829954893125!2d-96.31003088438847!3d39.21828073557019!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87be7c1138c23205%3A0x2d3680569e47468!2sMt+Calvary+Lutheran+Church!5e0!3m2!1sen!2sus!4v1551252178633" width="300" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>
			<div class="footer-follow">
				<div class="footer-follow__social">
					<p class="footer-follow__title">Follow Us<p>
					<a href="https://www.facebook.com/MtCalvaryLutheranChurch" rel="noopener" target="_blank" class="fa fa-facebook-square fa-large"></a>
				</div>
				<div class="footer-follow__newsletter">
					<p class="footer-follow__title">Newsletter Signup<p>
					<!-- Begin Constant Contact Inline Form Code -->
					<div class="ctct-inline-form" data-form-id="95ae6606-e394-4b93-a23a-f06c24878cc2"></div>
					<!-- End Constant Contact Inline Form Code -->
				</div>
			</div>
		</div>

		<div class="site-info">
			<a href="/wp-login.php" rel="nofollow">Admin Login</a>
			<span>|</span>
			<?php printf( __( 'Website %1$s by %2$s', 'mclc' ), 'Developed', '<a href="http://weare502.com" rel="designer">502 Media Group</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php do_action('website_after'); ?>

<?php wp_footer(); ?>
<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "54e7adc06b36e9eb3a67625d4e9fef1e"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->

</body>
</html>
