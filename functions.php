<?php
/**
 * Mt. Calvary Lutheran Church functions and definitions
 *
 * @package Mt. Calvary Lutheran Church
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'mclc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mclc_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Mt. Calvary Lutheran Church, use a find and replace
	 * to change 'mclc' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'mclc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'mclc' ),
		'secondary' => __('Secondary Menu', 'mclc'),
		'footer' => __('Footer Menu', 'mclc')
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'video', 'link', 'audio'
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'mclc_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // mclc_setup
add_action( 'after_setup_theme', 'mclc_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function mclc_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'mclc' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'mclc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mclc_scripts() {
	wp_enqueue_style( 'mclc-style', get_stylesheet_directory_uri() . '/css/style.css' );

	wp_enqueue_script( 'mclc-superfish', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), '20120206', true );

	wp_enqueue_script( 'mclc-hoverintent', get_template_directory_uri() . '/js/hoverIntent.js', array('mclc-superfish'), '20120206', true );

	wp_enqueue_script( 'mclc-supersubs', get_template_directory_uri() . '/js/supersubs.js', array('mclc-superfish'), '20120206', true );

	wp_enqueue_script( 'mclc-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery', 'off-canvas-sidebars', 'mclc-superfish'), '20120206', true );

	wp_enqueue_script( 'mclc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mclc_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

add_image_size( 'page-photo', 960, 240, array('center', 'center') );

add_editor_style('css/editor-style.css');




