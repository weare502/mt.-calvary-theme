<?php
/**
 * Home Page
 * @package Mt. Calvary Lutheran Church
 */

get_header(); ?>

	<div id="primary" class="content-area">

	<div id="home-video">

	<?php $hero = get_field('home_hero_section');
		if ( $hero == 'video' ) : ?>
			<iframe src='<?php echo trailingslashit( get_field('home_video_link') ); ?>' frameborder='0' allowfullscreen></iframe>
		<?php elseif ( $hero == 'image' ) : ?>
			<img src="<?php the_field('home_hero_image'); ?>" class="home-hero-image">
		<?php else : ?>
			<?php $slideshow_id = get_field('home_slideshow'); ?>
			<?php echo do_shortcode('[metaslider id="' . $slideshow_id . '"]'); ?>
		<?php endif; ?>

	</div>
		<div class="home-links">

			<?php if ( have_rows('home_ctas') ): while ( have_rows('home_ctas') ): the_row(); ?>
			<section>
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('text_snippet'); ?></p>
				<a href="<?php the_sub_field('link'); ?>" class="button"><?php the_sub_field('link_text'); ?></a>
			</section>

			<?php endwhile; endif; ?>
			<!-- <section>
				<h3>Listen to Services</h3>
				<p>Catch up with the congregation with this weeks sermon.</p>
				<a href="#" class="button">Link Here</a>
			</section>
			<section>
				<h3>Connect</h3>
				<p>Catch up with the congregation with this weeks newsletter.</p>
				<a href="#" class="button">Link Here</a>
			</section> -->
		</div>

		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php // get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					// if ( comments_open() || get_comments_number() ) :
					// 	comments_template();
					// endif;
					// 
					// ?modestbranding=1;controls=2;showinfo=0;rel=0;theme=light;color=white;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
