<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Mt. Calvary Lutheran Church
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" rel="icon" type="image/x-icon" />
<?php wp_head(); ?>
<style type="text/css" class="options-css">

	<?php if ( is_front_page() ) : ?>
		/* .main-navigation .nav-menu > li:hover:after {
			content: '';
			width: 300%;
		}
		.main-navigation .nav-menu > li:first-child:hover:after {
			<?php // if ( !empty(get_field('options_seek_image', 'options')) ) : // this wont work on >phpv5.5 ?>
				background: url(<?php the_field('options_seek_image', 'options'); ?>);
				background-size: cover;
			<?php // endif; ?>
		}
		.main-navigation .nav-menu > li:nth-child(2):hover:after {
			<?php // if ( !empty(get_field('options_join_image', 'options')) ) : ?>
				background: url(<?php the_field('options_join_image', 'options'); ?>);
				background-size: cover;
			<?php // endif; ?>
		}
		.main-navigation .nav-menu > li:nth-child(3):hover:after {
			<?php // if ( !empty(get_field('options_connect_image', 'options')) ) : ?>
				background: url(<?php the_field('options_connect_image', 'options'); ?>);
				background-size: cover;
			<?php // endif; ?>
		} */
	<?php endif; ?>

</style>
</head>

<body <?php body_class(); ?>>
<?php do_action('website_before'); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'mclc' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri() . '/img/logo.png'; ?>" alt="Mt. Calvary Lutheran Church" /></a></h1>
			<nav class="secondary-nav">
				<?php wp_nav_menu( array('theme_location' => 'secondary') ); ?>
				<form action="/" method="GET" class="search-form">
					<input type="text" placeholder="Search..." name="s" />
					<input type="submit" value="Search" class="button" />
				</form>
			</nav>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php _e( 'Menu', 'mclc' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
