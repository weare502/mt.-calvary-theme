/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
// ( function() {
// 	var container, button, menu;

// 	container = document.getElementById( 'site-navigation' );
// 	if ( ! container ) {
// 		return;
// 	}

// 	button = container.getElementsByTagName( 'button' )[0];
// 	if ( 'undefined' === typeof button ) {
// 		return;
// 	}

// 	menu = container.getElementsByTagName( 'ul' )[0];

// 	// Hide menu toggle button if menu is empty and return early.
// 	if ( 'undefined' === typeof menu ) {
// 		button.style.display = 'none';
// 		return;
// 	}

// 	menu.setAttribute( 'aria-expanded', 'false' );

// 	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
// 		menu.className += ' nav-menu';
// 	}

// 	button.onclick = function() {
// 		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
// 			container.className = container.className.replace( ' toggled', '' );
// 			button.setAttribute( 'aria-expanded', 'false' );
// 			menu.setAttribute( 'aria-expanded', 'false' );
// 		} else {
// 			container.className += ' toggled';
// 			button.setAttribute( 'aria-expanded', 'true' );
// 			menu.setAttribute( 'aria-expanded', 'true' );
// 		}
// 	};
// } )();

// Menus remove extra anchor tags

jQuery("ul#menu-footer.menu > li > a").contents().unwrap().wrap('<span></span>');

// Wrap iFrames in a container for responsive embeds
jQuery('iframe[src*="youtube.com"]').wrap('<div class="embed-container"></div>');
jQuery('iframe[src*="youtube.com"]').attr('src', function( i, val ) {
  										return val + "?modestbranding=1;controls=2;showinfo=0;rel=0;theme=light;color=white;autohide=1;";
  									});
var $navli = jQuery('.main-navigation .nav-menu > li');
$navli.click(function(){
	jQuery(this).toggleClass('clicked');
});
jQuery('*').not($navli).click(function(){
	$navli.removeClass('clicked');
});

var body = jQuery('body');
var breakpoint = 500;
var sf = jQuery('ul.sf-menu');

	// enable superfish when the page first loads if we're on desktop
	sf.supersubs({
		// minWidth:	12,	 // minimum width of submenus in em units
		maxWidth:	35,	 // maximum width of submenus in em units
		extraWidth:	1	
	}).superfish({
		speed: 'fast',
		delay: 400,
		cssArrows: false
	});

// jQuery(window).resize(function() {
// 	if(body.width() >= breakpoint && !sf.hasClass('sf-js-enabled')) {
// 		// you only want SuperFish to be re-enabled once (sf.hasClass)
// 		sf.superfish('init');
// 	} else if(body.width() < breakpoint) {
// 		// smaller screen, disable SuperFish
// 		sf.superfish('destroy');
// 	}
// });

// Initialize Slidebars
var controller = new slidebars();
controller.init();

jQuery("#main-menu-1 > li > a ").contents().unwrap().wrap('<span></span>');
jQuery("#main-menu-1 > li > .sub-menu > li > a[href='#'] ").contents().unwrap().wrap('<span></span>');

// Toggle Slidebars
jQuery( '.menu-toggle' ).on( 'click', function ( event ) {
  // Stop default action and bubbling
  event.stopPropagation();
  event.preventDefault();

  // Toggle the Slidebar with id 'id-1'
  controller.toggle( 'ocs-main-menu' );
  body.toggleClass('menu-open');
} );