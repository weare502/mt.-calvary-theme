<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Mt. Calvary Lutheran Church
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( is_page(52) ){ echo "<p><a href='javascript:window.print()' class='fa fa-print'>&nbsp;Print this Calendar</a></p>"; } ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'mclc' ),
				'after'  => '</div>',
			) );
		?>
		<?php 	
		$children = wp_list_pages( 
							array(
								'title_li' => 'Other pages in this section',
								'child_of' => get_the_ID(),
								'echo' => 0,
							) 
						); 
		echo $children; ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'mclc' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
